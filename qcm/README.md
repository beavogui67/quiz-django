# Project : creation of a QCM

This project consists of creating a MCQ on microscopy image identification - biological subject.
The user has the choice to select the desired quiz (recognize types of microscopy or recognize the type of cell presented, the organelles presented).

## Requirements
### Creating a Python environment
```shell
#place it in the chosen directory
py -m venv venv 
cd venv\Scripts\activate
```
The activation of the environment is distinguished by the display (venv) or other, in front of all lines on the terminal.
<br><i> (venv) angelina: ~/Documents/...</i>

Remember to activate the environment each time you work on the project (unless it is done automatically like on pycharm)

### Creation of the Django project
#### <u>Django installation:</u>
https://docs.djangoproject.com/en/3.2/topics/install/
```shell
pip install django
django-admin --version
```
#### <u>Creation of the project QCM:</u>

```shell
django-admin startproject qcmproject
```
#### <u>Installation of freeze:</u>
https://pip.pypa.io/en/stable/cli/pip_freeze/
```shell
pip install django-freeze
pip install -r requirements.txt
pip freeze > requirement.txt
```
<b>django-freeze</b> generates the static version of your django site.
<br>The text file <b>requirements.txt</b> including following modules which are installed on project.

#### <u>Installation of django-framework:</u>
Django REST framework is a powerful and flexible toolkit for building Web APIs.
https://www.django-rest-framework.org/
```shell
pip install djanforestframework
```

#### <u>Installation of pillow:</u>
The Python Imaging Library adds image processing capabilities to your Python interpreter.
https://pillow.readthedocs.io/en/stable/
```shell
pip install pillow
```

#### <u>Launch of the project:</u>
```shell
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
```
<b>makemigrations</b> : which is responsible for creating new migrations based on the changes you have made to your models.
<br><b>migrate</b> : which is responsible for applying and unapplying migrations.
<br><b>runserver</b> : Starts a lightweight development Web server on the local machine

#### <u>Installation of cros-header:</u>
A Django App that adds Cross-Origin Resource Sharing (CORS) headers to responses. This allows in-browser requests to your Django application from other origins.
Or to communicate between js and Django.
https://pypi.org/project/django-cors-headers/

```shell
pip install django-cors-headers
```

### React-js 
https://fr.reactjs.org/ 
<br>For the visual part of the project, I decided to use React js, a JavaScript library to manage the visual part of our applications in a reactive way.

```shell
sudo apt install nodejs
sudo apt install npm
npm start
```
<b>nodejs:</b> is a server-side JavaScript runtime environment that executes JavaScript code.
<br><b>npm:</b> default package manager of Node.js 
<br><b>start:</b> Runs the app in the development mode.
<br>Open http://localhost:3000 to view it in the browser.

#### <u>Installation of axios in the frontend:</u>
Axios est une bibliothèque JavaScript très populaire utilisée pour effectuer des requêtes HTTP, qui fonctionne à la fois sur les plateformes Browser et Node.js.
https://flaviocopes.com/node-axios/
```shell
npm install axios
```
#### <u>Installation of bootstrap:</u>
The use of boostrap allows to get the CSS and JavaScript compiled, the source code, included in our package manager, here npm.
https://getbootstrap.com/docs/5.0/getting-started/download/
```shell
npm install bootstrap
```







