/*
Creation of the App.
for more informations :
    https://www.taniarascia.com/getting-started-with-react/
*/

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';


ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

