import api_interface from "./api_interface";
import {API_URL} from "../Constantes/api_url";

export const reqGetQuestions = () => api_interface(API_URL + 'api_qcm/question/', {}, 'GET')
export const reqGetImages = (page) => api_interface(API_URL + 'api_qcm/img/?page=' + page, {}, 'GET')
export const reqLogin = (email, password) => api_interface(API_URL + 'api/token/',
    {email: email, password: password}, 'POST')
export const reqRegister = (email, user_name, password) => api_interface(API_URL + 'api/user/create/', {
    email: email,
    user_name: user_name,
    password: password
}, 'POST')
export const reqGetQuestionsImgReponse = (type) => api_interface(API_URL + 'api_qcm/showquestion?type=' + type, {}, 'GET')
