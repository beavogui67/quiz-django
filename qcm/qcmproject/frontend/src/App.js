/*
The main component in React which acts as a container for all other components.
*/

import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Qcm from "./Compenents/Qcm";
import Login from "./Compenents/Login";
import SignUp from "./Compenents/SignUp";
import Choice from "./Compenents/Choice";
import Images from "./Compenents/Images";

export default function App() {
    return (
        <div className="App">
            <Router>
                <Switch>
                    <Route path="/qcm" render={props => <Qcm {...props} />}/>
                    <Route path="/choice" render={props => <Choice {...props} />}/>
                    <Route path="/images" render={props => <Images {...props} />}/>
                    <Route path="/signup" render={props => <SignUp {...props} />}/>
                    <Route path="*" render={props => <Login {...props} />}/>
                </Switch>
            </Router>
        </div>
    );
}
