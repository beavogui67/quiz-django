import React, {Component} from 'react';
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import PageItem from 'react-bootstrap/PageItem'
import Pagination from 'react-bootstrap/Pagination'
import "./Choice.css"
import {reqGetImages, reqLogin} from "../api/url";
import {Container} from "@mui/material";


class Images extends Component {
    constructor(props) {
        super(props);
        this.state = {
            imgs: [],
            active: 0,
        }

    }

    componentDidMount() {
        if (localStorage.getItem('access_token') === 'null')
            this.props.history.push("/")
        localStorage.setItem('question_type', "");
        this.loadImages(this.state.active + 1)
    }

    loadImages(index) {
        reqGetImages(index).then((res) => {
            this.setState({imgs: res.data});
        })
    }

    render() {
        let active = this.state.active + 1;
        let items = [];
        let tot = (this.state.imgs.count / 9) < 1 ? 1 : this.state.imgs.count / 9;
        for (let number = 1; number <= tot; number++) {
            items.push(
                <Pagination.Item key={number} onClick={() => {
                    this.loadImages(number - 1)
                    this.setState({active: number - 1})
                }} active={number === active}>
                    {number}
                </Pagination.Item>,
            );
        }
        const paginationBasic = (
            <div>
                <Pagination>{items}</Pagination>
            </div>
        );
        console.log(this.state.imgs.results)
        return (
            <div className="choice container">
                <div className="w-100 p-3">
                    <Navbar className="color-nav" bg="dark" variant="dark" expand={"lg"} style={{width: "auto"}}>
                        <Container>
                            <Nav className="color-nav">
                                <Navbar.Brand href="/choice">Quiz</Navbar.Brand>
                                <Nav className="me-auto">
                                    <Nav.Link href="/images">Images</Nav.Link>
                                    <Nav.Link href="/signup">Sign Up</Nav.Link>
                                </Nav>
                            </Nav>
                        </Container>
                    </Navbar>
                </div>
                <div className="text">
                    <div className="card-body">
                        <h1>Image galleries</h1>
                        <br/>Find all the pictures of the quiz in order to give you more information about each picture.
                    </div>
                </div>
                <div className="row">
                    {paginationBasic}
                    <div className="col-lg-12 col-md-12">{
                        this.state.imgs.results &&
                        this.state.imgs.results.map((res) => {
                            return (
                                <div style={{marginTop: "5px"}} className="card content title_container">
                                    <div className="card-header title_container_header">
                                        organism: <b>{res.organism}</b>
                                    </div>
                                    <div className="card-body">
                                        <div style={{marginTop: "5px"}} className={"form-control"}><img src={res.chemin}/></div>
                                        <div style={{marginTop: "5px"}} className={"form-control"}><b>mode :</b> {res.mode}</div>
                                        <div style={{marginTop: "5px"}} className={"form-control"}><b>component : </b> {res.component}</div>
                                        <div style={{marginTop: "5px"}} className={"form-control"}><b>doi :</b> {res.doi}</div>
                                        <div style={{marginTop: "5px"}} className={"form-control"}><b>description :</b> {res.description}</div>
                                        <div style={{marginTop: "5px"}} className={"form-control"}><b>celltype :</b> {res.celltype}</div>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        );
    }
}

export default Images;