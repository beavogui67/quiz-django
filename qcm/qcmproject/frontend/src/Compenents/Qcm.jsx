import React, {Component} from 'react';
import "./Qcm.css"
import {reqGetQuestionsImgReponse} from "../api/url";
import {Box, Modal} from '@mui/material';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    pt: 2,
    px: 4,
    pb: 3,
};

class Qcm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            step: 1,
            score: 0,
            reponse_qcm: [],
            current_question: null,
            current_response: null,
            real_response: null,
            description: null,
            showanswer: false,
            show_result: false,
        }
    }

    componentWillMount() {

    }

    componentDidMount() {
        if (localStorage.getItem('access_token') === 'null')
            this.props.history.push("/")
        let questointype = localStorage.getItem('question_type')
        reqGetQuestionsImgReponse(questointype).then((res) => {
            let chaine = []
            if (res.data.length > 0) {
                res.data.map((qst) => {
                    chaine.push({
                        question: qst.id,
                        reponse: null
                    })
                });
                this.setState({
                    data: res.data,
                    current_question: res.data[0].id,
                    reponse_qcm: chaine
                }, () => this.init_reponse());
            }
        })
    }

    init_reponse(step = 1) {
        let mode = this.state.data[step - 1].images[0].mode
        let current_question = this.state.data[step - 1].id
        this.setState({real_response: mode, current_question: current_question})
        let ref = this.state.data[step - 1].reponses.find((x) => {
            return x.answer === mode;
        })
        this.setState({description: ref !== undefined ? ref.definition : null})
    }

    handleClose() {
        let new_step = this.state.step + 1;
        if (this.state.data.length >= new_step) {
            this.setState({
                step: new_step,
                showanswer: false,
            }, () => this.init_reponse(new_step));
        } else {
            //show_result
            this.setState({show_result: true, showanswer: false});
        }
    }

    handleCloseResult() {
        this.setState({show_result: false})
        this.props.history.push("/choice")
    }

    handleOpen() {
        let score = this.state.score;
        this.setState({showanswer: true, score: this.check_response() ? score + 1 : score})
    }

    check_response() {
        return this.state.current_response === this.state.real_response;
    }

    render() {
        return (
            <>

                <Modal
                    open={this.state.show_result}
                    onClose={() => this.handleCloseResult()}
                    aria-labelledby="parent-modal-title"
                    aria-describedby="parent-modal-description">
                    <Box sx={{...style, width: 400}}>
                        <h2 id="parent-modal-title">Your success rate
                            is {this.state.score * 100 / this.state.data.length}%</h2>
                    </Box>
                </Modal>
                <Modal
                    open={this.state.showanswer}
                    onClose={() => this.handleClose()}
                    aria-labelledby="parent-modal-title"
                    aria-describedby="parent-modal-description"
                >
                    <Box sx={{...style, width: 400}}>
                        <h2 id="parent-modal-title">{this.check_response() ? "Congratulation!!!" : "You will succeed, try again!!!"} </h2>
                        <p id="parent-modal-description">
                            the correct answer was : <b>{this.state.real_response}</b>
                            <br/>
                            <hr/>
                            {this.state.description}
                        </p>
                    </Box>
                </Modal>
                <div style={{display: "flex"}}>
                    <div style={{width: "45%", margin: "auto"}}>
                        <div className="MuiBox-root jss408">
                            <div className="MuiBox-root jss409">
                                <div style={{opacity: "1"}}>
                                    <div style={{opacity: "1"}}>
                                        <div className="MuiBox-root jss496">
                                            <div className="MuiBox-root jss500">
                                                <div
                                                    className="MuiLinearProgress-root MuiLinearProgress-colorPrimary jss497 MuiLinearProgress-determinate"
                                                    role="progressbar" aria-valuenow="20" aria-valuemin="0"
                                                    aria-valuemax="100">
                                                    <div
                                                        className="MuiLinearProgress-bar jss498 MuiLinearProgress-barColorPrimary MuiLinearProgress-bar1Determinate"
                                                        style={{transform: "translateX(-80%)"}}>
                                                    </div>
                                                </div>
                                                <p className="MuiTypography-root jss499 MuiTypography-body1">Question :
                                                    {this.state.step}/{this.state.data.length}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div style={{
                                        display: "flex",
                                        width: "100%",
                                        flexDirection: "row",
                                        flexWrap: "wrap"
                                    }}>
                                        {
                                            this.state.data[this.state.step - 1] &&
                                            this.state.data[this.state.step - 1].images.map((obj, i) => {
                                                if(i<4)
                                                return (
                                                    <img key={i} style={{width: "50%"}} src={obj.chemin}
                                                         alt={obj.description}>
                                                    </img>
                                                )
                                            })
                                        }
                                    </div>

                                    <div style={{opacity: "1"}}>
                                        <div className="MuiBox-root jss503 jss412">
                                            <div className="MuiBox-root jss504 jss410">
                                                <div className="MuiBox-root jss505">
                                                    {
                                                        this.state.data[this.state.step - 1] &&
                                                        <p className="MuiTypography-root jss506 MuiTypography-body1">
                                                            <b>{this.state.data[this.state.step - 1].question}</b>
                                                        </p>
                                                    }
                                                    <div className="MuiBox-root jss507">
                                                        <div
                                                            className="MuiGrid-root MuiGrid-container MuiGrid-spacing-xs-2"
                                                            style={{width: "100%", display: "flex"}}>
                                                            {
                                                                //si la data existe
                                                                this.state.data[this.state.step - 1] &&
                                                                //parcourir la data
                                                                this.state.data[this.state.step - 1].reponses.map((obj, i) => {
                                                                    //ici ont verifie si la reponse est selectionner par l'utilisateur
                                                                    let ref = this.state.reponse_qcm.find((x) => {
                                                                        return x.reponse === obj
                                                                    })
                                                                    return (
                                                                        <div
                                                                            key={i}
                                                                            onClick={() => {
                                                                                let qst = this.state.reponse_qcm;
                                                                                let foundIndex = qst.findIndex(x => x.question === this.state.current_question);
                                                                                qst[foundIndex].reponse = obj
                                                                                this.setState({
                                                                                    reponse_qcm: qst,
                                                                                    current_response: obj.answer,
                                                                                })

                                                                            }}
                                                                            className="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-6"
                                                                            style={{width: "50%"}}>
                                                                            <div
                                                                                // si la reponse est selectionner on l'entour de bleu
                                                                                className={ref !== undefined ?
                                                                                    "MuiPaper-root MuiCard-root jss508 jss510 MuiPaper-elevation1 MuiPaper-rounded bordered" :
                                                                                    "MuiPaper-root MuiCard-root jss508 jss510 MuiPaper-elevation1 MuiPaper-rounded"
                                                                                }>
                                                                                <div className="MuiBox-root jss512">
                                                                                    <p className="MuiTypography-root jss513 jss514 MuiTypography-body1">
                                                                                        {/*generer les numeros de question 123->ABC*/}
                                                                                        {(i + 10).toString(36).toUpperCase()}
                                                                                    </p>
                                                                                    <p className="MuiTypography-root jss509 jss511 MuiTypography-body1">
                                                                                        {obj.answer}</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    )
                                                                })
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="MuiBox-root jss548">
                                                    <div>
                                                        <button
                                                            className="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedPrimary"
                                                            tabIndex="0" type="button" style={{margin: "0px 8px;"}}
                                                            onClick={() => this.handleOpen()}
                                                        >
                                                            <span className="MuiButton-label">Check</span>
                                                            <span className="MuiTouchRipple-root">

                                                        </span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default Qcm;