import React, {Component} from 'react';
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import PageItem from 'react-bootstrap/PageItem'

import "./Choice.css"
import {reqLogin} from "../api/url";
import {Container} from "@mui/material";


class Choice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
        }

    }

    componentDidMount() {
        if (localStorage.getItem('access_token') === 'null')
            this.props.history.push("/")
        localStorage.setItem('question_type', "");
    }

    render() {
        return (

            <div className="choice container">
                <div className="w-100 p-3">
                    <Navbar bg="dark" variant="dark"  expand={"lg"} style={{ width: "auto" }} >
                      <Container>
                        <Nav className="color-nav">
                            <Navbar.Brand href="/choice">Quiz</Navbar.Brand>
                            <Nav className="me-auto">
                              <Nav.Link href="/images">Images</Nav.Link>
                              <Nav.Link href="/signup">Sign Up</Nav.Link>
                            </Nav>
                          </Nav>
                      </Container>
                    </Navbar>
                </div>
                <div className="text">
                    <div className="card-body">
                        <h2>Welcome, ready to learn?</h2>
                        <br/>This quiz was made so that you can go deeper and get started in identifying microscopic images
                        at the cellular level. <br/>The images were retrieved via the <a href="http://www.cellimagelibrary.org/"><b>Cell Image Library</b></a> webpage,
                        an accessible resource database that contains images of cells, capturing a wide variety of organisms,
                        cell types and cellular processes.<br/>
                        Have fun, explore the galleries or test your knowledge !
                    </div>
                </div>
                <div className="row" style={{paddingTop: "100px"}}>
                    <div className="col-lg-5 col-md-7"
                         onClick={() => {
                             localStorage.setItem('question_type', "microscopy");
                             this.props.history.push("/qcm?type=microscopy")
                         }}>
                        {/*<Image source={{uri: imageUrl}} />*/}
                        <div className="panel border bg-white">
                            <div className="" style={{marginTop: "12px", textAlign: "center"}}>
                                <img src="https://www.pngmart.com/files/17/Vector-Microscope-PNG-File.png" alt="new"
                                width="100" height="100" loading={"lazy"}/>
                                <h3 className="pt-3 font-weight-bold"> Microscopy</h3>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-5 col-md-7"
                         onClick={() => {
                             localStorage.setItem('question_type', "component");
                             this.props.history.push("/qcm?type=component")
                         }}>
                        <div className="panel border bg-white">
                            <div className="" style={{marginTop: "12px", textAlign: "center"}}>
                                 <img src="https://atlas-content-cdn.pixelsquid.com/stock-images/animal-cell-AEo4QMD-600.jpg" alt="new"
                                width="100" height="100" loading={"lazy"}/>
                                <h3 className="pt-3 font-weight-bold"> Cell Type</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Choice;