import React, {Component} from 'react';

import "./SignUp.css"
import { reqRegister} from "../api/url";

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            user_name: "",
            password: "",
        }

    }

    componentDidMount() {

    }

    render() {
        return (
            <div className="login container">
                <div className="row">
                    <div className="offset-md-2 col-lg-5 col-md-7 offset-lg-4 offset-md-3">
                        <div className="panel border bg-white">
                            <div className="panel-heading">
                                <h3 className="pt-3 font-weight-bold">Sign Up</h3>
                            </div>
                            <div className="panel-body p-3">
                                <form action="login_script.php" method="POST">
                                    <div className="form-group py-2">
                                        <div className="input-field">
                                            <span className="far fa-user p-2"></span>
                                            <input type="text"
                                                   value={this.state.email}
                                                   onChange={(e) => {
                                                       this.setState({email: e.target.value})
                                                   }}
                                                   placeholder="Email" required/>
                                        </div>
                                    </div>
                                    <div className="form-group py-2">
                                        <div className="input-field">
                                            <span className="far fa-user p-2"></span>
                                            <input type="text"
                                                   value={this.state.user_name}
                                                   onChange={(e) => {
                                                       this.setState({user_name: e.target.value})
                                                   }}
                                                   placeholder="Username" required/>
                                        </div>
                                    </div>
                                    <div className="form-group py-1 pb-2">
                                        <div className="input-field"><span className="fas fa-lock px-2"></span>
                                            <input type="password"
                                                   value={this.state.password}
                                                   onChange={(e) => {
                                                       this.setState({password: e.target.value})
                                                   }}
                                                   placeholder="Enter your Password" required/>
                                        </div>
                                    </div>
                                    <div
                                        className="btn btn-primary btn-block mt-3"
                                        onClick={() => {
                                            reqRegister(
                                                this.state.email,
                                                this.state.user_name,
                                                this.state.password
                                            ).then((res) => {
                                                this.props.history.push('/');
                                            }).catch((except) => {
                                                console.log(except)
                                            })
                                        }}
                                    >Sign up
                                    </div>
                                    <div className="text-center pt-4 text-muted">
                                        Don't have an account ?
                                        <a href="#" onClick={() => {
                                            localStorage.setItem('access_token', null);
                                            localStorage.setItem('refresh_token', null);
                                            this.props.history.push("/")
                                        }}> Sign In</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SignUp;