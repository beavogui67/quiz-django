"""qcm URL Configuration

File that allows to map URL path expressions to Python functions
For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
"""
from django.conf.urls import url
from django.urls import include
from qcm import views
from rest_framework import routers
# REST framework adds support for automatic URL routing to Django, and provides you with a simple, quick and
# consistent way of wiring your view logic to a set of URLs.

router = routers.DefaultRouter()
router.register(r'question', views.QuestionViewSet)  # URL to initialize the question table/model
router.register(r'reponse', views.ReponseViewSet) # URL to initialize the reponse table/model
router.register(r'img', views.ImageViewSet) # URL to initialize the image table/model
urlpatterns = [
    url(r'^showquestion?$', views.ShowQuestionViewSet.as_view(), name='show_question'),
    url('', include(router.urls)),
]
