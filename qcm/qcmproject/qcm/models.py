"""qcm Model Configuration

Creation of models for the database
For more information please see:
    https://docs.djangoproject.com/fr/2.2/topics/db/models/
"""

from django.core.validators import FileExtensionValidator
from django.db import models

from qcmproject import settings


class Question(models.Model):
    question = models.TextField()
    type = models.CharField(default=None, max_length=120, null=False)
    # imagefield = models.TextField()
    point = models.IntegerField(default=1)

    # n_answer = models.IntegerField()
    # n_image = models.IntegerField()
    @property
    def n_image(self):
        nb = Image.object.get(id=self.id).count()
        return nb


class Image(models.Model):
    question = models.ForeignKey(Question, related_name="images", on_delete=models.CASCADE)
    chemin = models.ImageField(
        upload_to='images',
        null=False,
        validators=[FileExtensionValidator(allowed_extensions=['jpeg', 'jpg', 'png', 'gif'])]
    )
    name = models.CharField(default=None, max_length=120, null=False)
    mode = models.CharField(default=None, max_length=120, null=False)
    celltype = models.CharField(default=None, max_length=120, null=True)
    component = models.CharField(default=None, max_length=120, null=True)
    doi = models.CharField(default=None, max_length=120, null=True)
    organism = models.CharField(default=None, max_length=120, null=True)
    description = models.TextField(null=False)


class Reponse(models.Model):
    question = models.ForeignKey(Question, related_name="reponses", on_delete=models.CASCADE)
    answer = models.CharField(default=None, max_length=120)
    definition = models.TextField(null=False)


class Score(models.Model):
    question = models.ForeignKey(Question, related_name="score", on_delete=models.CASCADE)
    point = models.IntegerField(default=0)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='qcm_score')
