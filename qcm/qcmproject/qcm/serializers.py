""" qcm Serializer Configuration

Serializers allow complex data such as querysets and model instances to be converted to native Python datatypes that can
then be easily rendered into JSON, XML or other content types.
For more information please see:
    https://www.django-rest-framework.org/api-guide/serializers/
"""

from rest_framework import serializers
from qcm.models import Image, Reponse, Question


class ImageSerializer(serializers.ModelSerializer):
    """
    Serialize the Image model
    """
    class Meta:
        """
        Internal class is a convenient namespace for data shared between instances of class
        """
        model = Image
        # fields = ["chemin", "name", "mode", "celltype", "component", "doi", "organism", "description"]
        fields = '__all__'


class ReponseSerializer(serializers.ModelSerializer):
    """
    Serialize the Reponse model
    """
    class Meta:
        model = Reponse
        fields = '__all__'


class QuestionSerializer(serializers.ModelSerializer):
    """
    Serialize the Question model
    """
    class Meta:
        model = Question
        fields = '__all__'


class AllQuestionSerializer(serializers.ModelSerializer):
    """
    Serialize all model
    """
    images = ImageSerializer(read_only=True, many=True)
    reponses = ReponseSerializer(read_only=True, many=True)
    nb_image = serializers.ReadOnlyField(source="n_image", read_only=True)

    class Meta:
        model = Question
        fields = (
            "id",
            "question",
            "type",
            "point",
            "images",
            "reponses",
            "nb_image"
        )
