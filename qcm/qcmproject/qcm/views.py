"""qcm Views Configuration

Creation of the different views
For more information please see:
    https://docs.djangoproject.com/fr/2.2/topics/http/views/
"""

from django.db.migrations import serializer
from django.shortcuts import render

from qcm.models import Reponse, Question, Image
from qcm.serializers import ReponseSerializer, QuestionSerializer, ImageSerializer, AllQuestionSerializer

from rest_framework import viewsets, views, status
from rest_framework.response import Response


# ViewSets define the view behavior.
# status and Reponse, allows you to specify the error code when a validation error occurs in the serializer


class QuestionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Question to be viewed or edited.
    """
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer


class ImageViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Image to be viewed or edited.
    """
    queryset = Image.objects.all()
    serializer_class = ImageSerializer


class ReponseViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Reponse to be viewed or edited.
    """
    queryset = Reponse.objects.all()
    serializer_class = ReponseSerializer


class ShowQuestionViewSet(views.APIView):
    """
    API endpoint that show Question to be viewed or edited.
    """

    def get(self, request, *args, **kwargs):
        selector = request.GET.get('type', None)
        if selector:
            question_list = Question.objects.all().filter(type=selector)
            serialised_data = AllQuestionSerializer(question_list, many=True, context={"request": request})
            return Response(serialised_data.data, status=status.HTTP_200_OK)
        else:
            return Response("Please specify the type of question", status=status.HTTP_400_BAD_REQUEST)
