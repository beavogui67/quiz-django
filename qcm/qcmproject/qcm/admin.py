from django.contrib import admin
from . import models


@admin.register(models.Score)
class AuthorAdmin(admin.ModelAdmin):
    list_display = ('question', 'id', 'point', 'author')


admin.site.register(models.Question)
